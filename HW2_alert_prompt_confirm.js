
/* 1. Які існують типи даних у Javascript?

    В Javascript существует 8 типов данных:
  7 примитивных: 
- Number (все числа от -(2^53-1) и до +(2^53-1));
- BigInt (позволяет проводить вычисления с числами больше (2^53-1), т.е. больше чем 16 значные числа)
- String ("abc" , "555abc55"...);
- boolean (true, false);
- null ("определенное пустое значение");
- undefined ("неопределенное значение", если переменная объявлена и ей небыло присвоено значение, то по умолчанию ее значением будет undefined, функция в которой нет return возвращает undefined);
- symbol (самый редкоиспользуемый тип, используется для уникальных идентификаторов)
  1 более сложный, не примитивный тип:
- Object (сюда же входят массивы, классы);
*/

/*  2. У чому різниця між == і ===??

  Это операторы сравнения. Двойное равно (==) производит неявное приведение типов и если удается привести к одному типу, затем выполняется сравнение величин.
  Тройное равно (===) сравнивает 2 величины не приводя их к одному типу, по этому ели типы сравниваемых величин разные, то этот оператор сразу возвращает false.  
*/

/*  3. Що таке оператор? 

   Операторы - это набор специальных символов при использовании которых запускаются определенные встроенные функции JS, 
   которые выполняют определенные действия с операндами, к которым эти операторы были применены и возвращают результат этих действий. 
*/


// 1

const askUserName = (text, value) => prompt(text || "Введіть ваше ім'я", value || "");
const askUserAge = (text, value) => prompt(text || "Введіть ваш вік", value || 20);

let userName = askUserName();
let userAge = askUserAge();

nameValidation(userName);
ageValidation(userAge);

const accessDenied = () => alert("You are not allowed to visit this website");
const accessConfirm = () => confirm("Are you sure you want to continue?");
const welcome = (name) => alert(`Welcome, ${name.trim()}!`);

(function accessValidation(name, age) {
  if (age < 18) {
    accessDenied();
  } else if (age > 18 && age <= 22) {
    accessConfirm() ? welcome(name) : accessDenied();
  } else {
    welcome(name);
  }
})(userName, userAge);

function nameValidation(name) {
  if (/\d/g.test(name)) {
    let validUserName = nameValidation(askUserName("Ви ввели не корректне і'мя", name));
    return validUserName;
  } else if (!name) {
    let validUserName = nameValidation(askUserName("Ви не ввели і'мя", name));
    return validUserName;
  }
  return name;
};

function ageValidation(age) {
  if (isNaN(age)) {
    let validUserAge = ageValidation(askUserAge("Введіть корректний вік", age));
    return validUserAge;
  }
  return age;
};

